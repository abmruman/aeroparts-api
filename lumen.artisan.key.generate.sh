#!/bin/bash
#
# Author: ABM Ruman <abm.ruman@gmail.com>


# Substitute for `php artisan key:generate` in lumen (requires a 32bit string as APP_KEY)
# Copy `.env.example` to `.env` first, if you havn't already.

# Generates 32bit pseudo-random alphaneumaric string (https://gist.github.com/earthgecko/3089509)
# and replaces it with APP_KEY value

# Note: this regular expression is for testing alphaneumaric string only, it will not work with Laravel,
# Laravel uses base64 encoded string.


sed -i "s/^APP_KEY=[a-zA-Z0-9]*$/APP_KEY=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1 )/g" .env

